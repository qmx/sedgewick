#include <stdio.h>

static struct node {
  int key;
  struct node *next;
};
static struct node *head, *z, *t;
stackinit()
{
  head = (struct node *) malloc(sizeof(*head));
  z = (struct node*) malloc(sizeof(*z));
  head->next = z;
  head->key = 0;
  z->next = z;
}

push(int v)
{
  t = (struct node *) malloc(sizeof(*t));
  t->key = v;
  t->next = head->next;
  head->next = t;
}

int pop()
{
  int x;
  t = head->next;
  head->next = t->next;
  x = t->key;
  free(t);
  return x;
}

int main(void)
{
  stackinit();
  push(1);
  push(2);
  push(3);
  printf("%d", pop());
  printf("%d", pop());
  printf("%d", pop());
  return 0;
}
